import requests as rq
import random
DEBUG = False

class wordleBot:
    words = [word.strip() for word in open("5letters.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(wordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(wordleBot.creat_url, json=creat_dict)

        self.choices = [w for w in wordleBot.words[:] if is_unique(w)]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(wordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']
        attempt = 1

        while not won and attempt < 7:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            if not self.choices:
                print("No more possible choices left.")
                break
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
            attempt += 1

        if won:
            print("Secret is", choice, "found in", len(tries), "attempts")
        else:
            print("Failed to find the secret in 6 attempts.")
        print("Route is:", " => ".join(tries))

        
    def update(self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            for i, (c, f) in enumerate(zip(choice, feedback)):
                if f == 'G' and word[i] != c:
                    return False
                if f == 'Y' and (c not in word or word[i] == c):
                    return False
                if f == 'B' and c in word:
                    return False
            return True
        
        self.choices = [w for w in self.choices if matches_feedback(w, choice, feedback)]

game = wordleBot("Team")
game.play()


