
import random

import random

def read_file(file_name: str) -> list:
    with open(file_name, 'r') as file:
        return file.read().strip().split()

def get_feedback(guess, target):
    feedback = []
    for g, t in zip(guess, target):
        if g == t:
            feedback.append('G')
        elif g in target:
            feedback.append('Y')
        else:
            feedback.append('R')
    return feedback

def check_win(guess, target):
    return guess == target

def play_wordle(file_name: str):
    words = read_file(file_name)
    target = random.choice(words)
    
    while True:
        guess = input("Enter your guess: ").strip().lower()
    
        for guess in words:
            guess = guess.strip().lower()
        
            if guess not in words:
                continue
        
            feedback = get_feedback(guess, target)
            print(f"Guess: {guess}, Feedback: {feedback}")
        
            if check_win(guess, target):
                print("WIN!")
                return guess
    
        print("No correct guess found.")
        return None

print(play_wordle("5letters.txt"))



