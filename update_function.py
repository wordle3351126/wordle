def update(self: Self, choice: str, feedback: str):
        def matches_feedback(word: str, choice: str, feedback: str) -> bool:
            for i, (c, f) in enumerate(zip(choice, feedback)):
                if f == 'G' and word[i] != c:
                    return False
                if f == 'Y' and (c not in word or word[i] == c):
                    return False
                if f == 'B' and c in word:
                    return False
            return True
